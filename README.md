## Requirements

Please have NodeJS installed with NPM.

## Installation

Install `grunt-cli` and `bower`


```
npm install -g grunt-cli bower
```

Install all of the listed node modules and bower components

```
npm install
```

```
bower install
```

## Usage

To serve the elections and view them on your browser, use the `serve` task in grunt

```
grunt serve
```

Default server is at `http://localhost:9000` but it should open on its own.

## 'API'

The 'API', which serves a single static json file 'elections.json', is located at `/api/elections.json`.
There is an artificial delay of (2 seconds) added to the response to mimic a 'real' API.

For development, you can alter this delay by adding a prefix to the URL, like so:
`/delay/{time}/api/elections.json`
Where time is specificied in milliseconds.

For example, the following will make the delay 10 seconds:
`/delay/10000/api/elections.json`
Where as the following will make the delay 0 seconds:
`/delay/0/api/elections.json`
