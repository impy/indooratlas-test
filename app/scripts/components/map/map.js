(function() {
  'use strict';

  /**
   * @ngdoc function
   * @name challengeApp.components:map
   * @description
   * # Map Component
   * Map
   */
  angular.module('challengeApp')
    .directive('map', map);

  function map() {
    return {
      restrict: 'A',
      transclude: true,
      scope: {},
      bindToController: {
        options: '=',
        controlFn: '=',
        onDestroy: '&',
        onMapMove: '&'
      },
      controller: MapController,
      controllerAs: 'ctrl',
      template: '<map-container></map-container><div ng-transclude></div>'
    };
  }

  // @ngInject
  function MapController($scope, $element, $timeout) {
    var defaultOptions = { center: { lat: 0, lng: 0 }, zoom: 3 };
    var options = angular.extend(defaultOptions, this.options);

    this.map = new google.maps.Map($element.find('map-container')[0], options);
    this.markers = [];
    this.listeners = [];
    this.$scope = $scope;
    this.$timeout = $timeout;

    // Register events for destructor
    if (typeof this.onMapMove === 'function') {
      this.listeners.push(this.bind('dragend', this.onMapMove));
      this.listeners.push(this.bind('resize', this.onMapMove));
      this.listeners.push(this.bind('zoom_changed', this.onMapMove));
    }

    typeof this.onDestroy === 'function' && this.listeners.push($scope.$on("$destroy", this.onDestroy));

    // Register destructor
    $scope.$on("$destroy", angular.bind(this, this.destructor));

    // Share map function to speciefied object
    this.controlFn = this.controlFn || {};
    this.controlFn.setCenter = angular.bind(this, this.setCenter);
    this.controlFn.fitBounds = angular.bind(this, this.fitBounds);
  }


  MapController.prototype.bind = function(name, fn) {
    var ctrl = this;
    var listener = this.map.addListener(name, function() {
      var $map = this;

      ctrl.$timeout(function() { fn({ $map: $map }) });
    });

    return angular.bind(ctrl, function() {
      ctrl.map && ctrl.map.removeListener(listener);
    });
  };


  MapController.prototype.setCenter = function(center, zoom) {
    var ctrl = this;

    ctrl.map.setCenter({
      lat: Number(center[1] || 0),
      lng: Number(center[0] || 0)
    });

    ctrl.map.setZoom(Number(zoom || 3));
  };


  MapController.prototype.fitBounds = function() {
    var ctrl = this;

    if (ctrl.markers.length === 0) {
      return;
    }

    var bounds = new google.maps.LatLngBounds();

    for (var i = 0; i < ctrl.markers.length; i++) {
     bounds.extend(ctrl.markers[i].getPosition());
    }

    ctrl.map.fitBounds(bounds);
  };


  MapController.prototype.destructor = function() {
    var ctrl = this;

    ctrl.listeners.map(function(unsubscribe) {
      unsubscribe();
    });

    ctrl.listeners = [];
  };
}());
