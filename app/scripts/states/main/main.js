(function() {

  'use strict';

  /**
   * @ngdoc function
   * @name challengeApp.states:mainState
   * @description
   * # mainState
   * Main state of the challengeApp
   */
  angular.module('challengeApp')
    .directive('mainState', mainState);

  function mainState() {
    return {
      restrict: 'A',
      scope: {},
      controller: function() {},
      controllerAs: 'ctrl',
      bindToController: true,
      templateUrl: 'states/main/main.html'
    };
  }
}());
