(function() {
  'use strict';

  /**
   * @ngdoc function
   * @name challengeApp.components:analysisFileList
   * @description
   * # analysisFileList Component
   * Handles list of items
   */
  angular.module('challengeApp')
    .directive('loadingBar', loadingBar);

  function loadingBar() {
    return {
      restrict: 'A',
      scope: {},
      controller: LoadingBarController,
      controllerAs: 'ctrl',
      bindToController: true,
      templateUrl: 'components/loading_bar/loading_bar.html'
    };
  }

  // @ngInject
  function LoadingBarController($scope) {
    var ctrl = this;

    ctrl.isLoading = true;

    $scope.$on('$stateChangeStart', function(event, toState, toParams, fromState, fromParams) {
      ctrl.isLoading = true;
    });

    $scope.$on('$stateChangeSuccess', function(event, toState, toParams, fromState, fromParams) {
      ctrl.isLoading = false;
    });

     $scope.$on('$stateChangeError', function(event, toState, toParams, fromState, fromParams) {
       ctrl.isLoading = false;
    });
  }
}());
