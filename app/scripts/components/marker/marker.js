(function() {
  'use strict';

  /**
   * @ngdoc function
   * @name challengeApp.components:marker
   * @description
   * # marker Component
   * Google Maps marker component
   */
  angular.module('challengeApp')
    .directive('marker', marker);

  function marker() {
    return {
      restrict: 'A',
      scope: {},
      controller: MarkerController,
      controllerAs: 'ctrl',
      bindToController: {
        active: '=',
        marker: '=',
        onClick: '&'
      },
      template: '<div></div>'
    };
  }

  // @ngInject
  function MarkerController($element, $scope) {
    var ctrl = this;
    ctrl.parentController = $element.parent().controller('map');

    $scope.$watch(function() { return ctrl.active; }, function (value) {
      ctrl.markerElement.setAnimation(value ? google.maps.Animation.BOUNCE : false);
    });

    if (ctrl.parentController) {
      ctrl.markerElement = new google.maps.Marker({
        map: ctrl.parentController.map,
        clickable: true,
        position: new google.maps.LatLng(ctrl.marker.location.coordinates[1], ctrl.marker.location.coordinates[0])
      });

      ctrl.markerElement.addListener('click', function() {
        ctrl.onClick({
          $marker: this,
          $item: ctrl.marker
        });

        $scope.$apply();
      });

      ctrl.parentController.markers.push(ctrl.markerElement);
    }


    $scope.$on("$destroy", angular.bind(ctrl, ctrl.destructor));
  }

  MarkerController.prototype.destructor = function() {
    if (this.parentController && this.markerElement) {
      var idx = this.parentController.markers.indexOf(this.markerElement);

      this.markerElement.setMap(null);
      idx > -1 && this.parentController.markers.splice(idx, 1);
    }
  };
}());
