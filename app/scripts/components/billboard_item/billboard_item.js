(function() {
  'use strict';

  /**
   * @ngdoc function
   * @name challengeApp.components:billboardItem
   * @description
   * # billboardItem Component
   * Handles item of list
   */
  angular.module('challengeApp')
    .directive('billboardItem', billboardItem);

  function billboardItem() {
    return {
      restrict: 'A',
      scope: {},
      controller: BillboardItemController,
      controllerAs: 'ctrl',
      bindToController: {
        active: '=',
        billboardItem: '=',
        onClick: '&'
      },
      templateUrl: 'components/billboard_item/billboard_item.html'
    };
  }

  function BillboardItemController() {}

  BillboardItemController.prototype.getIcon = function() {
    var ctrl = this;
    switch(ctrl.billboardItem.report_status) {
      case 'accepted': return 'check';
      default: return 'warning';
    }
  };

    BillboardItemController.prototype.handleClick = function() {
      var ctrl = this;

      this.onClick({
        $item: ctrl.billboardItem
      });
    };
}());
