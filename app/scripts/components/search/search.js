(function() {
  'use strict';

  /**
   * @ngdoc function
   * @name challengeApp.components:search
   * @description
   * # Search in category Component
   * Handles search of elements
   */
  angular.module('challengeApp')
    .directive('search', search);

  function search() {
    return {
      restrict: 'A',
      scope: {},
      controller: SearchController,
      controllerAs: 'ctrl',
      bindToController: {
        search: '=',
        onSearch: '&'
      },
      templateUrl: 'components/search/search.html'
    };
  }

  // @ngInject
  function SearchController() {
    var ctrl = this;
    ctrl.value  = ctrl.search ? String(ctrl.search) : '';
  }

  SearchController.prototype.handleSearch = function(value) {
    typeof this.onSearch === 'function' && this.onSearch({ $value: value });
  };
}());
