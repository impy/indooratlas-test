(function() {
  'use strict';

  /**
   * @ngdoc function
   * @name challengeApp.components:billboardList
   * @description
   * # billboardList Component
   * Handles list of items
   */
  angular.module('challengeApp')
    .directive('billboardList', billboardList);

  function billboardList() {
    return {
      restrict: 'A',
      scope: {},
      transclude: true,
      controller: BillboardListController,
      controllerAs: 'ctrl',
      bindToController: {},
      templateUrl: 'components/billboard_list/billboard_list.html'
    };
  }

  function BillboardListController() {
    'ngInject';
    var ctrl = this;
  }
}());
