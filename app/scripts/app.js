(function() {
  'use strict';

  /**
   * @ngdoc overview
   * @name challengeApp
   * @description
   * # challengeApp
   *
   * Main module of the application.
   */
  angular
    .module('challengeApp', [
      'ngAnimate',
      'ngCookies',
      'ngResource',
      'ui.router',
      'ngMaterial',
      'ngSanitize'
    ])
    .config(function ($stateProvider, $urlRouterProvider, $locationProvider) {
      'ngInject';

      $urlRouterProvider.otherwise('/');
      $locationProvider.html5Mode(true);

      $stateProvider
        .state('main', {
          url: '',
          template: '<div main-state></div>',
          resolve: {
            filesStore: function(ApiStore) {
              'ngInject';
              return ApiStore.fetch();
            }
          }
        })
        .state('main.map', {
          url: '/?id&center&zoom&search',
          reloadOnSearch: false,
          template: '<div map-state></div>'
        });
    });
}());
