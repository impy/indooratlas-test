(function() {

  'use strict';

  /**
   * @ngdoc function
   * @name challengeApp.states:mapState
   * @description
   * # mapState
   * Map state of the challengeApp
   */
  angular.module('challengeApp')
    .directive('mapState', mapState);

  function mapState() {
    return {
      restrict: 'A',
      scope: {},
      controller: MapStateController,
      controllerAs: 'ctrl',
      bindToController: true,
      templateUrl: 'states/map/map.html'
    };
  }


  function MapStateController($stateParams, $scope, $state, ApiStore, $timeout) {
    'ngInject';
    var ctrl = this;
    var center = ($stateParams.center || '').split(',');

    ctrl.id = Number($stateParams.id);
    ctrl.mapControl = {};
    ctrl.$scope = $scope;
    ctrl.$state = $state;
    ctrl.$timeout = $timeout;
    ctrl.search = $stateParams.search;
    ctrl.items = ApiStore.getState();

    ctrl.mapOptions = {
      center: {
        lat: Number(center[0] || 49.95802 ),
        lng: Number(center[1] || 14.491407)
      },
      zoom: Number($stateParams.zoom || 8)
    };
  }


  MapStateController.prototype.updateState = function($map) {
    var ctrl = this;

    // Hold state on map update
    ctrl.$state.go('main.map', {
      id: ctrl.id ? ctrl.id : null,
      center: $map.getCenter().toUrlValue(),
      zoom: $map.getZoom()
    });
  };


  MapStateController.prototype.onItemClick = function(item, setCenter) {
    var ctrl = this;
    setCenter = typeof setCenter === 'undefined' ? true : setCenter;
    ctrl.id = item._id;

    if (setCenter) {
      ctrl.mapControl.setCenter(item.location.coordinates, 14 /*zoom*/);
    } else {
      // update only url
      ctrl.$state.go('main.map', {
        id: ctrl.id,
      });
    }
  };


  MapStateController.prototype.onSearch = function($value) {
    var ctrl = this;
    ctrl.$state.go('main.map', { 'search': $value });
    ctrl.search = $value;

    // Fit bounds on the next tick
    ctrl.$timeout(ctrl.mapControl.fitBounds);
  };
}());
