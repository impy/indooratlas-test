(function() {
  'use strict';

  /**
   * @ngdoc function
   * @name challengeApp.stores:ApiStore
   * @description
   * # Api Store
   * Store of fetched data from the API
   */
  angular.module('challengeApp')
    .service('ApiStore', ApiStore);

  // @ngInject
  function ApiStore($http) {
    var state = {};

    function getState() {
      return state;
    }

    function filterById(id) {
      if (typeof id === 'undefined' || id === '') {
        return;
      }

      return state.filter(function(item) { return item.id === id; })[0];
    }

    function setStateFromRequest(payload) {
      state = payload.data.data;
      return state;
    }

    function fetch() {
      return $http.get('/delay/1000/api/elections.json')
        .then(setStateFromRequest);
    }

    return {
      getState: getState,
      filterById: filterById,
      fetch: fetch
    };
  }
}());
